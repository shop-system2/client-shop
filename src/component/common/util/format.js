

const FormatAmount = (value) => {
  return `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export {
  FormatAmount
}