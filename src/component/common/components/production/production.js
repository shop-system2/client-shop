


const Production = (props) => {


    return (
        <div className="box-production">
            <a href="#" className="box-img">
                <img src={props.img} alt={props.alt} />
            </a>
        </div>
    )
}
const ProductionModal = (props) => {


    return (
        <div className="box-production">
            <a className="box-img">
                <img src={props.img} alt={props.alt} />
            </a>
        </div>
    )
}

export default Production
export {
  ProductionModal
}