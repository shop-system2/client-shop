import React from 'react';
import { heart } from 'assets/icon/icon';

const HeaderBoundingBox = (props) => {
    return (
        <div className="header-box">
            <div className="header-box-flag">
                {Object.keys(props.tag || []).map((item) => {
                    if (props.tag[item] === undefined || props.tag[item] === '') return <></>;
                    return <span key={item} className={`flag ${item}`}>{props.tag[item]}</span>
                })}
            </div>
            <div className="header-box-wish">
                <a href="#" className="btn-wish"><img src={heart} alt="heart" /></a>
            </div>
        </div>
    )
}

export default HeaderBoundingBox