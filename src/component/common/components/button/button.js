import React from 'react';
import { arrowRight } from 'assets/icon/icon';


const ButtonBox = (props) => {

  return (
    <div className="box-btn full bar-full box-btn-tranform">
      <a href="#" className="btn-box-info bar" onClick={props.onClick}>
        <span className="btn-content">{props.des} <img src={arrowRight} alt={arrowRight} /></span>
      </a>
    </div>
  )
}

const ButtonStory = (props) => {
  return (
    <div className="box-btn-story bar-converse-full">
      <a href="#" className="btn-box-info-story bar-converse" onClick={props.onClick}>
        <span className="btn-content-story">{props.des} <img src={arrowRight} alt={arrowRight} /></span>
      </a>
    </div>
  )

}

const ButtonStoryConverse = (props) => {
  return (
    <div className="box-btn-converse bar-full">
      <a href="#" className="btn-box-converse-info bar" onClick={props.onClick}>
        <span className="btn-content">{props.des} <img src={arrowRight} alt={arrowRight} /></span>
      </a>
    </div>
  )

}

const GeneralButton = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <ButtonStory des={props.des} onClick={props.onClick} />
      </div>
    </div>
  )

}

const GeneralButtonConverse = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <ButtonStoryConverse des={props.des} onClick={props.onClick} />
      </div>
    </div>
  )

}

const GeneralDisplayButton = (props) => {
  return (
    <div className="box-btn-story bar-converse-full">
      <a href="#" className="btn-box-info-story bar-converse" onClick={props.onClick}>
        <span className="btn-content-story">{props.leftImg && <img src={props.leftImg} alt="img-left" />} {props.des} {props.rightImg && <img src={props.rightImg} alt="img-right" />}</span>
      </a>
    </div>
  )
}

const GeneralDisplayButtonBox = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <GeneralDisplayButton des={props.des} onClick={props.onClick} leftImg={props.leftImg} rightImg={props.rightImg} />
      </div>
    </div>
  )
}

const GeneralDisplayButtonConverse = (props) => {
  return (
    <div className="box-btn-converse bar-full">
      <a href="#" className="btn-box-converse-info bar" onClick={props.onClick}>
        <span className="btn-content">{props.leftImg && <img src={props.leftImg} alt="img-left" />} {props.des} {props.rightImg && <img src={props.rightImg} alt="img-right" />}</span>
      </a>
    </div>
  )
}

const GeneralDisplayButtonBoxConverse = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <GeneralDisplayButtonConverse des={props.des} onClick={props.onClick} leftImg={props.leftImg} rightImg={props.rightImg} />
      </div>
    </div>
  )
}


const GeneralDisplayButtonWithIcon = (props) => {
  return (
    <div className="box-btn-story bar-converse-full">
      <a href="#" className="btn-box-info-story bar-converse" onClick={props.onClick}>
        <span className="btn-content-story">{props.leftImg} {props.des} {props.rightImg}</span>
      </a>
    </div>
  )
}

const GeneralDisplayButtonBoxWithIcon = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <GeneralDisplayButtonWithIcon des={props.des} onClick={props.onClick} leftImg={props.leftImg} rightImg={props.rightImg} />
      </div>
    </div>
  )
}

const GeneralDisplayButtonConverseWithIcon = (props) => {
  return (
    <div className="box-btn-converse bar-full">
      <a href="#" className="btn-box-converse-info bar" onClick={props.onClick}>
        <span className="btn-content">{props.leftImg} {props.des} {props.rightImg}</span>
      </a>
    </div>
  )
}

const GeneralDisplayButtonBoxConverseWithIcon = (props) => {
  return (
    <div className="general-wrap-btn">
      <div className="general-warp-btn-item">
        <GeneralDisplayButtonConverseWithIcon des={props.des} onClick={props.onClick} leftImg={props.leftImg} rightImg={props.rightImg} />
      </div>
    </div>
  )
}
export { ButtonStory, GeneralButton, GeneralButtonConverse, GeneralDisplayButton, GeneralDisplayButtonBox, GeneralDisplayButtonBoxConverse, GeneralDisplayButtonBoxWithIcon, GeneralDisplayButtonBoxConverseWithIcon };
export default ButtonBox
// export { ButtonStory, GeneralButton, GeneralButtonConverse };