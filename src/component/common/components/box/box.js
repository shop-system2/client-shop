import React, { useMemo, useState } from "react";
const { default: HeaderBoundingBox } = require("../headerBox/headerBox");
import Production from "component/common/components/production/production";
import Info from "../info/info";
import "../style.css";
import ButtonBox, { ButtonStory, GeneralButton } from "../button/button";
import ListProduct from "./listProd";
import ingredient1 from "assets/img/ingredient1.jpg";
import { heart, chatDots } from "assets/icon/icon";
import ItemModal from "../../../../pages/homePage/component/itemModal";

const BoundingBox = (props) => {
  const [visible, setVisible] = useState(false);

  const openModal = () => {
    setVisible(true);
  };
  return (
    <>
      <div className="bounding-box">
        <HeaderBoundingBox tag={props.tag} />
        <Production img={props.img} alt={props.alt} />
        <Info
          info={props.description}
          price={props.amount.price}
          sale={props.amount.sale}
        />
        {/* <ButtonBox des="Mua hàng" /> */}
        <ButtonBox des="Mua hàng" onClick={openModal} />
      </div>
      <ItemModal
        detail={{ star: 4, like: 100 }}
        setVisible={setVisible}
        visible={visible}
        productID={props && props.productID}
      />
    </>
  );
};

const StoryContent = (props) => {
  return (
    <div className="story">
      <div className="story-wrap">
        <div className="story-wrap-img">
          <img src={ingredient1} alt="1" />
        </div>
        <div className="story-wrap-info">
          <div className="story-wrap-info-content">
            <p className="story-wrap-info-content-title">{props.title}</p>
            <p className="story-wrap-info-content-description">{props.info}</p>
          </div>
          <div className="story-wrap-btn">
            <div className="story-warp-btn-item">
              <ButtonStory des="Xem chi tiết" />
            </div>
          </div>
        </div>
      </div>
      <ListProduct detail={props.detail} />
    </div>
  );
};

const ReviewContent = (props) => {
  const renderStar = useMemo(() => {
    const star = [];
    for (let i = 0; i < 5; i++) {
      if (i < props.detail.star) {
        star[i] = <span className="review-star">*</span>;
        continue;
      }
      star[i] = <span className="review-star">||</span>;
    }
    return star;
  }, []);

  const renderTags = useMemo(() => {
    const tags = [];
    Object.keys((props.detail && props.detail.tag) || []).forEach((item) => {
      tags[item] = <span className="review-tag">{props.detail.tag[item]}</span>;
    });
    return tags;
  }, []);

  return (
    <div className="review">
      <div className="review-wrap">
        <div className="review-wrap-header">
          <div className="review-icon">
            <img src={props.detail.icon_url} alt="icon" />
          </div>
          <div className="review-title">
            <h5>{props.detail.name}</h5>
            <div className="review-header-info">
              <div>{props.detail.major}</div>
              <div>{props.detail.date}</div>
            </div>
          </div>
        </div>
        <div className="review-wrap-image">
          <div className="review-image">
            <img src={props.detail.image_url} alt="image" />
          </div>
        </div>
        <div className="review-wrap-star">{renderStar}</div>
        <div className="review-wrap-info">
          <h5>{props.detail.title}</h5>
          <p>{props.detail.description}</p>
          <div className="review-tag-wrap">{renderTags}</div>
        </div>
        <div className="review-warp-footer">
         <div className="review-general-footer">
            <img src={heart} alt="star" />
            <span>{props.detail.star}</span>
          </div>
          <div className="review-general-footer">
            <img src={heart} alt="like" />
            <span>{props.detail.number_like}</span>
          </div>
          <div className="review-general-footer">
            <img src={chatDots} alt="comment" />
            <span>{props.detail.comment}</span>
          </div> 
        </div>
      </div>
    </div>
  );
};

const AboutMeContent = (props) => {
  return (
    <div className="about">
      <div className="about-wrap">
        <div className="about-wrap-img">
          <img src={props.img} alt="brand" />
        </div>
        <h5>{props.title}</h5>
        <p>{props.description}</p>
        <GeneralButton des="Xem chi tiết" />
      </div>
    </div>
  );
};

const ServiceContent = (props) => {
  return (
    <div className="service">
      <div className="service-wrap">
        <div className="service-wrap-icon">
          <img src={props.img} alt="icon" />
        </div>
        <h6>{props.title}</h6>
        <p>{props.description}</p>
        <div className="service-wrap-footer">
          <a href="#" className="service-wrap-link">
            Xem chi tiết
          </a>
        </div>
      </div>
    </div>
  );
};

export default BoundingBox;
export { StoryContent, ReviewContent, AboutMeContent, ServiceContent };
