



const Counter = (props) => {
  const decrease = () => props.decrease()
  const increase = () => props.increase()

  return (
    <>
      <div className="item-body-counter-container">
        <div className="item-body-counter">
          <a type="button" onClick={decrease}>
            -
          </a>
          <p>{props.counter}</p>
          <a type="button" onClick={increase}>
            +
          </a>
        </div>
      </div>
    </>
  )
}


export {
  Counter
}