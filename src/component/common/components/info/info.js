const Info = (props) => {
  let sale = props.sale;
  if (props.sale === "" || props.sale === "0" || props.sale === 0) sale = undefined;
  const isSale = sale === undefined ? "origin" : "sale";
  return (
    <div className="info-box">
      <div className="info">
        <p>{props.info}</p>
      </div>
      <div className="amount">
        {sale && <p className="sale">VND {props.sale}</p>}
        <p className="origin"> VND {props.price}</p>
      </div>
    </div>
  );
};
const FitInfo = (props) => {
  return (
    <div className="fit-info">
      <img src={props.image} alt="image" />
      {props.info}
    </div>
  );
};
export { FitInfo };

export default Info;
