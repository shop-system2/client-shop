import React from "react";
import ItemTagComponent from "component/tag/tagItem";

const TagComponent = (props) => {
  return (
    <>
      <ItemTagComponent tag={props && props.listTag} />
    </>
  );
};

export default TagComponent;
