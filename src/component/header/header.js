// import logo from 'assets/img/logo.svg';
import logo from 'assets/img/logo.jpg';
import { person, cart } from 'assets/icon/icon.jsx';
// import { geoAlt, search } from 'assets/icon/icon.jsx';

import { Product } from 'component/header/modal/modal';
import { useState } from 'react';
// import {  About } from 'component/header/modal/modal';
import {CartHeader} from './cart/cart';

const Header = () => {
  const [showCart, setShowCart] = useState(false);

  const handleShowCart = () => setShowCart(true)
  return (
    <>
      <div className="header-content">
        <nav>
          <ul>
            <li className="under-line">
              <a href="#">Sản phẩm</a>
              <Product />
            </li>
            {/* <li className="under-line"><a href="#">INNISTAR</a></li> */}
            {/* <li className="under-line"><a href="#">Sự kiện</a></li>
                    <li className="under-line"><a href="#">Thành viên</a></li> */}
            {/* <li className="under-line">
                        <a href="#">Về INNISTAR</a>
                        <About />
                    </li> */}
          </ul>
        </nav>
        <div id="header-logo" >
          <a href="/"><img src={logo} alt="logo" /></a>
        </div>
        <ul>
          {/* <li><a href="#" className="icon-decoration"><img className="img img1" src={geoAlt} alt="geoAlt" /></a></li> */}
          {/* <li><a href="#" className="icon-decoration"><img className="img img2" src={search} alt="search" /></a></li> */}
          <li><a href="#" className="icon-decoration"><img className="img img3" src={person} alt="person" /></a></li>
          <li><a href="#" className="icon-decoration" onClick={handleShowCart}><img className="img img4" src={cart} alt="cart" /></a></li>
        </ul>
      </div>
      <CartHeader show={showCart} setShow={setShowCart}/>
    </>
  )
}

export default Header;