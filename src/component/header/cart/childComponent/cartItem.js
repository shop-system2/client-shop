const { Counter } = require("component/common/components/counter/counter")
const { FormatAmount } = require("component/common/util/format")




const CartItem = (props) => {
  const decrease = () => {
    props.decreaseCart(props.idx);
  }

  const increase = () => {
    props.increaseCart(props.idx);
  }

  const remove = () => {
    props.remove(props.idx)
  }

  return (
    <>
      <div className="cart-item-container">
        <div className="item-left">
          <div className="item-left-img">
            {/* <img src={props.img} alt="img-left" /> */}
            {props.cartData.img}
          </div>
        </div>
        <div className="item-right">
          <div className="item-right-close" onClick={() => remove()}>
            <i class="ri-close-line"></i>
          </div>
          <div className="item-right-title">
            {props.cartData.title}
          </div>
          <Counter decrease={decrease} increase={increase} counter={props.cartData.count} />
          <div className="item-right-price">
            {FormatAmount(props.cartData.sum)} VND
          </div>
        </div>
      </div>
    </>
  )
}

export {
  CartItem
}