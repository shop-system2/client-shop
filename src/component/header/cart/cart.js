
import Offcanvas from 'react-bootstrap/Offcanvas'
import { CartItem } from './childComponent/cartItem';
import { useCallback, useState, useMemo } from 'react';
import { GeneralDisplayButtonBoxWithIcon, GeneralDisplayButtonBoxConverseWithIcon } from "component/common/components/button/button";
import "./style.css";
const { FormatAmount } = require("component/common/util/format")
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment, clearData, removeItem } from 'redux/features/cartSlice';

const dataCart = [
  {
    img: "abc",
    count: 1,
    title: "abcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    price: "260000",
    sum: "260000",
  },
  {
    img: "abc",
    count: 1,
    title: "abc",
    price: "260000",
    sum: "260000",
  }
]

const CartHeader = (props) => {
  const cartData = useSelector((state) => state.cart.cartData)
  const dispatch = useDispatch()
  // const [cartData, setCartData] = useState(dataCart);
  const [memoInd, setMemoInd] = useState(false);

  const handleClose = () => props.setShow(false);

  const decreaseCart = (idx) => {
    if (cartData[idx].count === 0) {
      return
    }
    dispatch(decrement(idx))
    setMemoInd(!memoInd)
  }

  const increaseCart = (idx) => {
    dispatch(increment(idx))
    setMemoInd(!memoInd)
  }

  const removeItemCart = (idx) => {
    dispatch(removeItem(idx))
    setMemoInd(!memoInd)
  }

  const cartItemRender = useMemo(() => {
    return <>{Object.keys(cartData).map(key => {
      return <CartItem key={key} decreaseCart={decreaseCart} increaseCart={increaseCart} cartData={cartData[key]} idx={key} remove={removeItemCart} />
    })}
    </>
  }, [memoInd])

  return (
    <Offcanvas show={props.show} onHide={handleClose} placement="end" style={{ width: "600px" }}>
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>Giỏ hàng/ Cart</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
        <div className="cart-body-container">
          {cartItemRender}
          <div className="cart-item-remove">
            <a id="remove" onClick={() => { dispatch(clearData()); setMemoInd(!memoInd) }}>
              <i class="ri-delete-bin-2-line"></i>
              Xóa giỏ hàng
            </a>
          </div>
          <div className="cart-item-sum">
            <p>Tổng số tiền</p>
            <p>{cartData.length > 0 && FormatAmount(Object.keys(cartData).reduce((previousCartDetail, currentCartDetail) => {
              return parseInt(cartData[previousCartDetail].sum) + parseInt(cartData[currentCartDetail].sum)
            }))} VND</p>
          </div>
          <div className="cart-item-btn">
            <GeneralDisplayButtonBoxConverseWithIcon des="Mua tất cả sản phẩm" rightImg={<i class="ri-arrow-right-line"></i>} />
          </div>
          <div className="cart-item-btn">
            <GeneralDisplayButtonBoxWithIcon des="Xem thêm sản phẩm" />
          </div>
        </div>
      </Offcanvas.Body>
    </Offcanvas>
  );
}

export {
  CartHeader
}