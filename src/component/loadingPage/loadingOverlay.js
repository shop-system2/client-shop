import { Modal, Spinner } from "react-bootstrap";

const LoadingOverlay = (props) => {
  return (
    <Modal
      show={props.show}
      size="sm"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <Spinner animation="border" role="status" />
        &nbsp;
        <span>Loading...</span>
      </Modal.Body>
    </Modal>
  );
};

export default LoadingOverlay;
