import alarm from 'assets/icon/alarm.svg';
import geoAlt from 'assets/icon/geo-alt.svg';
import search from 'assets/icon/search.svg';
import person from 'assets/icon/person.svg';
import cart from 'assets/icon/cart.svg';
import arrowRight from 'assets/icon/arrow-light.svg';
import arrowDownShort from 'assets/icon/arrow-down-short.svg';
import heart from 'assets/icon/heart.svg';
import caretLeftFill from 'assets/icon/caret-left-fill.svg';
import caretRightFill from 'assets/icon/caret-right-fill.svg';
import chatDots from 'assets/icon/chat-dots.svg';
import facebook from 'assets/icon/facebook.svg';
import instagram from 'assets/icon/instagram.svg';
import youtube from 'assets/icon/youtube.svg';
import PE1101_attimg_origin from 'assets/icon/PE1101_attimg_origin.svg';
import PE1102_attimg_origin from 'assets/icon/PE1102_attimg_origin.svg';
import PE1104_attimg_origin from 'assets/icon/PE1104_attimg_origin.svg';
import PE1201_attimg_origin from 'assets/icon/PE1201_attimg_origin.svg';

export {
    alarm, geoAlt, search, person, cart, arrowRight, arrowDownShort, heart,
    caretLeftFill, caretRightFill, chatDots, facebook, instagram, youtube,
    PE1101_attimg_origin, PE1102_attimg_origin, PE1104_attimg_origin, PE1201_attimg_origin
};

