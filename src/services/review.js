import { instBackRequest } from "./request";
import * as API from "./api";

async function GetListReviewCommon() {
  return instBackRequest.get(API.GetReviewCommonAPI, {
    headers: {
      "Content-Type": "application/json",
    },
  });
}

export { GetListReviewCommon };
