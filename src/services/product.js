import { instBackRequest } from "./request";
import * as API from "./api";

async function GetListProductPerferentail(request) {
  return instBackRequest.get(API.GetProductPerferentailAPI, {
    headers: {
      "Content-Type": "application/json",
    },
    data:request
  });
}

async function GetProductDetailByID(request) {
  return instBackRequest.get(API.GetProductDetailByID, {
    headers: {
      "Content-Type": "application/json",
    },
    params: request
  });
}

async function GetListProductTrending(request) {
  return instBackRequest.get(API.GetProductTrendingAPI, {
    headers: {
      "Content-Type": "application/json",
    },
    data:request
  });
}

export { GetListProductPerferentail,GetListProductTrending,GetProductDetailByID };
