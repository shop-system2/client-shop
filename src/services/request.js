import axios from "axios";

const instGatewayRequest = axios.create({
  baseURL: process.env.REACT_APP_GATEWAY_API_KEY,
});

const instBackRequest = axios.create({
  baseURL: process.env.REACT_APP_SHOP_BACK_CLIENT_API_KEY,
});
instBackRequest.interceptors.response.use((response) => {
  return response.data;
});

// cowRequest.interceptors.request.use( async (req) =>{
//     const jwt = await AsyncStorage.getItem('@jwt')
//     if(jwt) {
//         req.headers.Authorization =  `Bearer ${jwt}`
//     }else {
//         req.headers.Authorization =  null
//     }
//     return req
// })

export { instGatewayRequest, instBackRequest };
