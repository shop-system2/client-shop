import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "remixicon/fonts/remixicon.css";
import { store } from "./redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      {/* <BrowserRouter> */}
        {/* <Switch> */}
          {/* <Route path="/personal-portfolio">
            <PersonalPortfolio />
          </Route>
          <Route exact> */}
            <App />
          {/* </Route> */}
        {/* </Switch> */}
      {/* </BrowserRouter> */}
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
