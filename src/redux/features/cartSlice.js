import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  cartData: [
    {
      img: "abc",
      count: 1,
      title: "abcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      price: "260000",
      sum: "260000",
    },
    {
      img: "abc",
      count: 1,
      title: "abc",
      price: "260000",
      sum: "260000",
    }
  ]
}

export const cartSlice = createSlice({
  name: 'cartData',
  initialState: {
    cartData: [
      {
        img: "abc",
        count: 1,
        title: "abcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        price: "260000",
        sum: "260000",
      },
      {
        img: "abc",
        count: 1,
        title: "abc",
        price: "260000",
        sum: "260000",
      }
    ]
  },
  reducers: {
    increment: (state, action) => {
      const cartDetail = state.cartData[action.payload]
      cartDetail.count++
      let sum = parseInt(cartDetail.sum)
      sum += parseInt(cartDetail.price)
      cartDetail.sum = sum
      state.cartData[action.payload] = cartDetail
    },

    decrement: (state, action) => {
      const cartDetail = state.cartData[action.payload]
      if (cartDetail === 0) {
        return
      }
      cartDetail.count--
      let sum = parseInt(cartDetail.sum)
      sum -= parseInt(cartDetail.price)
      cartDetail.sum = sum
      state.cartData[action.payload] = cartDetail
    },

    removeItem: (state, action) => {
      state.cartData.splice(action.payload, 1)
    },

    clearData: (state) => {
      state.cartData = [];
    }
  },
})

export const { increment, decrement, clearData, removeItem } = cartSlice.actions

export default cartSlice.reducer