import { parse } from 'querystring';

export const getParamsQuery = () => {
    const { href } = window.location;
    const qsIndex = href.indexOf('?');
    const sharpIndex = href.indexOf('#');
  
    if (qsIndex !== -1) {
      if (qsIndex > sharpIndex) {
        return parse(decodeURIComponent(href.split('?')[1]));
      }
      return parse(decodeURIComponent(href.slice(qsIndex + 1, sharpIndex)));
    }
  
    return {};
  };