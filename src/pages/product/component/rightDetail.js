import React, { useCallback, useState, useEffect } from "react";
import TagComponent from "component/tag";
import { FitInfo } from "component/common/components/info/info";
import {
  caretRightFill,
  caretLeftFill,
  heart,
  PE1101_attimg_origin,
  PE1102_attimg_origin,
  PE1104_attimg_origin,
  PE1201_attimg_origin,
} from "assets/icon/icon";
import {
  GeneralDisplayButtonBox,
  GeneralDisplayButtonBoxConverse,
} from "component/common/components/button/button";
import { FormatAmount } from "component/common/util/format";
import { Route, useParams } from "react-router-dom";
import {getParamsQuery} from 'utils/utils'
import { GetProductDetailByID } from "services/product";


const ProductDetailRightComponent = () => {
  const renderFit = useCallback((fits) => {
    return (
      <div className="list-fit">
        {Object.keys(fits || []).map((key) => {
          switch (fits[key]) {
            case "DA":
              return <FitInfo image={PE1101_attimg_origin} info="Dưỡng ẩm" />;
            case "CLH":
              return (
                <FitInfo image={PE1102_attimg_origin} info="Chống lão hóa" />
              );
            case "DS":
              return <FitInfo image={PE1104_attimg_origin} info="Dưỡng sáng" />;
            case "MLD":
              return (
                <FitInfo image={PE1201_attimg_origin} info="Mọi loại da" />
              );
            default:
              return null;
          }
        })}
      </div>
    );
  }, []);

  // const [amountObj, setAmountObj] = useState({
  //   counter: 1,
  //   amount: 388000,
  // });

  const reduceDeal = () => {
    setAmountObj((state) => {
      return {
        counter: state.counter === 1 ? state.counter : state.counter - 1,
        amount: state.counter === 1 ? state.amount : state.amount - priceItem,
      };
    });
  };

  const increaseDeal = () => {
    setAmountObj((state) => {
      return {
        counter: state.counter + 1,
        amount: state.amount + priceItem,
      };
    });
  };

  const listDataTag = { promo: "promo" };
  const [productDetail, setProductDetail] = useState({});
  const [amountObj, setAmountObj] = useState({});
  const [priceItem, setPriceItem] = useState();

  const getProductDetail = async (id) => {
    let err;
    const respProductDetail = await GetProductDetailByID({ id }).catch(
      (errOut) => {
        err = errOut;
      }
    );
    if (err || respProductDetail.status_code != "ACCEPT") {
      // TODO handle call back request
      return;
    }
    setProductDetail(respProductDetail.product_detail);
    let price =
      respProductDetail.product_detail &&
      respProductDetail.product_detail.price;
    if (
      respProductDetail.product_detail.sell_price !== undefined &&
      respProductDetail.product_detail.sell_price !== "" && respProductDetail.product_detail.sell_price !== "0"
    ) {
      price =
        respProductDetail.product_detail &&
        respProductDetail.product_detail.sell_price;
    }
    setPriceItem(parseInt(price));
    setAmountObj({ counter: 1, amount: parseInt(price) });
  };


  useEffect(() => {
    const {id } = getParamsQuery()
    console.log("param", id);
    if (id) {
      getProductDetail(id)
    }
  }, []);

  return (
    <div>
      <div className="header-right">
        <TagComponent listTag={listDataTag} />

        <p>
          {productDetail.name}
        </p>

        <p>
        {productDetail.short_description}
        </p>
{/* 
        <p>
          Nước cân bằng từ lựu đỏ innisfree Jeju Pomegranate Revitalizing Toner
          giúp cân bằng độ ẩm, dưỡng sáng và chống oxy hóa để làn da luôn tươi
          trẻ.
        </p> */}

        {renderFit(["DA", "CLH", "MLD"])}

        <div className="item-modal-body-amount">

          <div className="item-modal-body-point">
            <h4>Điểm thưởng</h4>
            <ul>
              <li>
                Welcome <span>2%</span>
              </li>
              <li>
                Premium <span>3%</span>
              </li>
              <li>
                VIP <span>4%</span>
              </li>
            </ul>
          </div>
          
          <div className="item-modal-body-point">
            <h4>Số lượng</h4>
            <div className="item-modal-body-amount-info">
              <div className="item-modal-body-counter">
                <div className="item-modal-body-deal">
                  <a type="button" onClick={reduceDeal}>
                    -
                  </a>
                  <p>{amountObj.counter}</p>
                  <a type="button" onClick={increaseDeal}>
                    +
                  </a>
                </div>
              </div>
              <div className="item-modal-body-amount">
                <p className="item-modal-body-amount-display">
                  {FormatAmount(amountObj.amount)} VND
                </p>
              </div>
            </div>
          </div>
        </div>
  
        <div className="item-modal-body-btn-container">
          <div className="item-modal-body-btn">
              <GeneralDisplayButtonBoxConverse des="Thêm vào giỏ hàng" />
          </div>
        </div>
        
      </div>
    </div>
  );
};

export default ProductDetailRightComponent;
