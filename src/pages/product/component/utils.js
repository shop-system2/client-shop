import { caretRightFill, caretLeftFill } from "assets/icon/icon";

export const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: <img src={caretLeftFill} className="caret-left" />,
    nextArrow: <img src={caretRightFill} className="caret-right" />,
  };
  