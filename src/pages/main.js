import React from "react";
import Header from "component/header/header";
import Footer from "component/footer/footer";
import HomePage from "pages/homePage";
import LoginPage from "pages/loginPage";
import AboutPage from "pages/about";
import ProductDetailPage from "pages/product";
import { BrowserRouter, Route, Switch } from "react-router-dom";

const MainPage = (props) => {
  return (
    <div className="view">
      <div className="header">
        <Header />
      </div>
      <div className="content">
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/about">
              <AboutPage />
            </Route>
            <Route path="/product">
              <ProductDetailPage />
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
      <div className="main-content">
        <Footer />
      </div>
    </div>
  );
};

export default MainPage;
