import React from "react";

const CertificationBody = () => {
  return <div>
      <div id="phoneInput">
          <input  placeholder="Ví dụ: 0335280000 / 84336280000"/>
          <button >Gửi</button>  
      </div>
      <hr />
      <div id="phoneCode">
          <input placeholder="Ví dụ: 123456"/>
          <button>Xác nhận</button>  
      </div>
  </div>;
};

export default CertificationBody;
