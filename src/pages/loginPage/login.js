import React, { useState } from "react";
import { GeneralButton } from "component/common/components/button/button";
import "./style.css";
// import ModalForgotPass from './modal_forgot_pass';

const LoginContent = (props) => {
  const [openModal, setOpenModal] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    const user = document.getElementById("user");
    const password = document.getElementById("password");
  };

  const forgorPass = () => {
    setOpenModal(true);
  };

  return (
    <>
      <div className="main-login">
        <div className="main-login-content">
          <div className="main-login-content-box">
            <div className="sign-in-content">
              <h4>Đăng nhập thành viên</h4>
              <form onSubmit={onSubmit}>
                <input
                  type="email"
                  id="user"
                  placeholder="ex: 123456789/sample@beautifull.com"
                />
                <input
                  type="password"
                  id="password"
                  placeholder="Vui lòng nhập mật khẩu"
                />
                <a href="#" id="forget" onClick={forgorPass}>
                  <span>Bạn quên mật khẩu</span>
                </a>
                {/* <div className="sign-in-content-btn"> */}
                {/* <div className="sign-in-content-btn-box"> */}
                <GeneralButton des="Đăng nhập" />
                {/* </div> */}
                {/* </div> */}
              </form>
              <p>Đăng nhập nhanh/ Đăng ký bằng tài khoản mạng xã hội:</p>
              <p className="note">
                Có thể đăng nhập nhanh hoặc đăng ký thành viên mới bằng tài
                khoản mạng xã hội.
              </p>
              <p className="note">
                Thành viên beautifull chưa liên kết với tài khoản mạng xã hội có
                thể đăng nhập và thiết lập liên kết một cách nhanh chóng.
              </p>
            </div>
            <div className="sign-up-content">
              <h4>Đăng ký thành viên mới</h4>
              <p className="note">
                Đăng ký ngay để mua sắm dễ dàng hơn và tận hưởng thêm nhiều ưu
                đãi độc quyền cho thành viên beautifull.
              </p>
              {/* <GeneralButton des="Đăng ký thành viên" /> */}
              {/* </p> */}
              <div className="sign-up-content-btn">
                <GeneralButton
                  des="Đăng ký thành viên"
                  onClick={() => {
                    props.setShowRegisterModal(true);
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <ModalForgotPass openModal={openModal} setOpenModal={setOpenModal} /> */}
    </>
  );
};

export default LoginContent;
