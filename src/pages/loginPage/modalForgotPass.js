import React from "react";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import CertificationBody from './certificationBody';
// function rand() {
//   return Math.round(Math.random() * 20) - 10;
// }
function getModalStyle() {
  const top = 30 ;
  const left = 50;
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 1000,
    height: 500,
    backgroundColor: theme.palette.background.paper,
    // border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius:0.5,
  },
}));

const ModalForgotPass = ({ openModal, setOpenModal }) => {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  
  const handleClose = () => {
    setOpenModal(false);
  };

  return (
    <Modal
      open={openModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      onClose={handleClose}
    >
      <div style={modalStyle} className={classes.paper}>
        <div id='cerTop'>
            <span>X</span>
            <i/>
        </div>
        <p>Bạn quên mật khẩu?</p>
        <div is="cerOption">
            <span id="cerOptionPhone">
                <input type="radio" />
                <label>Số điện thoại</label>
            </span>
            <span id="cerOptionEmail">
                <input type="radio" />
                <label>Địa chỉ email</label>
            </span>
        </div>
        <hr />
        <CertificationBody />

      </div>
    </Modal>
  );
};

export default ModalForgotPass;
