import Header from "component/header/header";
const { default: LoginContent } = require("./login");
import RegisterModal from "./register";
import { useState } from "react";
import LoadingOverlay from "component/loadingPage/loadingOverlay";

const LoginPage = (props) => {
  const [isShowRegisterModal, setIsShowRegisterModal] = useState(false);
  const [isShowLoading, setIsShowLoading] = useState(false);

  return (
    <div className="view">
      <div className="header">
        <Header />
      </div>
      <div className="content">
        {/* <LoginContent /> */}
        <LoginContent
          setShowRegisterModal={setIsShowRegisterModal}
          setIsShowLoading={setIsShowLoading}
        />
        <RegisterModal
          show={isShowRegisterModal}
          setShow={setIsShowRegisterModal}
        />
      </div>
      <LoadingOverlay show={isShowLoading} handleClose={setIsShowLoading} />
    </div>
  );
};

export default LoginPage;
