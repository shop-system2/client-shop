import React from 'react';
import AboutContent from './component/content';
import './style.css';

const AboutPage = () => {
    return (
        <div className="main">
            <div className="main-content">
                <AboutContent />
            </div>
        </div>
    )
}

export default AboutPage