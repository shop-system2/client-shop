import React from "react";
import { useState, useEffect } from "react";
import { caretRightFill, caretLeftFill } from "assets/icon/icon";
import { GetListProductPerferentail } from "../../../services/product";
import {
  CatalogContent,
  CatalogDescriptionContent,
} from "component/common/components/catalog/catalog";
import Slider from "react-slick";
import BoundingBox from "component/common/components/box/box";
import prod1 from "assets/img/product/prod_1_1.png";
import prod2 from "assets/img/product/prod_1_2.png";
import { ACCEPT } from "../../../constants/const";
import prod3 from "assets/img/product/prod_1_3.png";
import {
  ReviewContent,
  AboutMeContent,
} from "component/common/components/box/box";
import iconReview from "assets/img/review/ico_superstar.png";
import imgReview from "assets/img/review/5004_10699_attimg_origin.jpeg";
import brand1 from "assets/img/brand/brand1.jpg";
import { Row, Col } from "react-bootstrap";
import {settings} from './utils';

const aboutDetail = [
  {
    img: brand1,
    title: "Giới thiệu về beautifull",
    sub_title:
      "beautifull được thành lập vào tháng 1 năm 2000, là thương hiệu mỹ phẩm thừa hưởng những tinh túy của thiên nhiên từ đảo Jeju, hòn đảo tràn ngập trong không khí trong lành và tươi mát. Jeju được UNESCO công nhận là di sản thiên nhiên thế giới đầu tiên của Hàn Quốc.",
  },
  {
    img: brand1,
    title: "Giới thiệu về beautifull",
    sub_title:
      "beautifull được thành lập vào tháng 1 năm 2000, là thương hiệu mỹ phẩm thừa hưởng những tinh túy của thiên nhiên từ đảo Jeju, hòn đảo tràn ngập trong không khí trong lành và tươi mát. Jeju được UNESCO công nhận là di sản thiên nhiên thế giới đầu tiên của Hàn Quốc.",
  },
];

const MainReviewContent = () => {
  return (
    <CatalogContent title="Về chúng tôi">
      <Row>
        {Object.keys(aboutDetail).map((index) => {
          return (
            <Col key={index}>
              <AboutMeContent
                img={aboutDetail[index].img}
                title={aboutDetail[index].title}
                description={aboutDetail[index].sub_title}
              />
            </Col>
          );
        })}
      </Row>
    </CatalogContent>
  );
};

export default MainReviewContent;
