import React from "react";
import { useState, useEffect } from "react";
import { caretRightFill, caretLeftFill } from "assets/icon/icon";
import {
  CatalogContent,
  CatalogDescriptionContent,
} from "component/common/components/catalog/catalog";
import Slider from "react-slick";
import BoundingBox from "component/common/components/box/box";
import prod1 from "assets/img/product/prod_1_1.png";
import prod2 from "assets/img/product/prod_1_2.png";
import { ACCEPT } from "../../../constants/const";
import prod3 from "assets/img/product/prod_1_3.png";
import {
  ReviewContent,
  AboutMeContent,
} from "component/common/components/box/box";
import iconReview from "assets/img/review/ico_superstar.png";
import imgReview from "assets/img/review/5004_10699_attimg_origin.jpeg";
import { settings } from "./utils";
import { GetListReviewCommon } from "../../../services/review";

const reviewDetail = [
  {
    icon: iconReview,
    name: "Garam",
    major: "Super Star",
    date: "2020-12-25",
    img: imgReview,
    star: 4,
    title: "Đánh giá CAO CẤP ẤM",
    description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
    tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
    like: 40,
    comment: 21,
  },
  {
    icon: iconReview,
    name: "Garam",
    major: "Super Star",
    date: "2020-12-25",
    img: imgReview,
    star: 4,
    title: "Đánh giá CAO CẤP ẤM",
    description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
    tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
    like: 40,
    comment: 21,
  },
  {
    icon: iconReview,
    name: "Garam",
    major: "Super Star",
    date: "2020-12-25",
    img: imgReview,
    star: 4,
    title: "Đánh giá CAO CẤP ẤM",
    description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
    tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
    like: 40,
    comment: 21,
  },
  {
    icon: iconReview,
    name: "Garam",
    major: "Super Star",
    date: "2020-12-25",
    img: imgReview,
    star: 4,
    title: "Đánh giá CAO CẤP ẤM",
    description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
    tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
    like: 40,
    comment: 21,
  },
];

const MainReviewContent = () => {
  const [reviews, setReview] = useState([]);

  const getListReview = async () => {
    let err;
    const respReview = await GetListReviewCommon().catch((errOut) => {
      err = errOut;
    });
    console.log("review:", respReview);

    if (err || (respReview && respReview.status_code != ACCEPT)) {
      return;
    }
    setReview(respReview.reviews);
  };
  useEffect(() => {
    getListReview();
  }, []);

  if (Object.keys(reviews || []).length == 0) {
    return <></>;
  } else {
    return (
      <CatalogContent title="Review">
        <CatalogDescriptionContent>
          <Slider {...settings}>
            {Object.keys(reviews || []).map((index) => {
              return <ReviewContent key={index} detail={reviews[index]} />;
            })}
          </Slider>
        </CatalogDescriptionContent>
      </CatalogContent>
    );
  }
};

export default MainReviewContent;
