import React from "react";
import Slider from "react-slick";

import iconReview from "assets/img/review/ico_superstar.png";
import imgReview from "assets/img/review/5004_10699_attimg_origin.jpeg";
import brand1 from "assets/img/brand/brand1.jpg";
import { AboutMeContent } from "component/common/components/box/box";
import {
  CatalogContent,
  CatalogDescriptionContent,
} from "component/common/components/catalog/catalog";
import { caretRightFill, caretLeftFill } from "assets/icon/icon";
import { Row, Col } from "react-bootstrap";
import MainCatalogContent from "./productPreferential";
import ReviewContent from "./review";
import AboutContent from "./about";
import ComponentProductTrending from "./productTrending";

const MainContent = (props) => {
  return (
    <>
      <div className="main-content-catalog">
        <MainCatalogContent title="SẢN PHẨM ƯU ĐÃI" setVisible={props.setVisible}/>
      </div>

      <div className="main-content-catalog">
        <ComponentProductTrending title="SẢN PHẨM BÁN CHẠY" setVisible={props.setVisible}/>
      </div>

      <div className="main-content-catalog">
        <ReviewContent />
      </div>
      <div className="main-content-catalog">
        <AboutContent />
      </div>
    </>
  );
};

export default MainContent;
