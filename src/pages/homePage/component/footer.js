import { facebook , youtube, instagram } from "assets/icon/icon";
// import { youtube, instagram } from "assets/icon/icon";
import logo from 'assets/img/logo.svg';
// import footMark from 'assets/img/foot_mark.png';


const footerTopDetail = [
    {
        "Sản phẩm": [
            // 'SẢN PHẨM BÁN CHẠY',
            // 'SẢN PHẨM MỚI',
            // 'ĐỘC QUYỀN ONLINE',
            // 'SẢN PHẨM ƯU ĐÃI',
            // 'Dòng sản phẩm',
            'Dưỡng da',
            'Trang điểm',
            'Tóc và cơ thể',
            'Nước hoa',
        ],
    },
    {
        "My innistar": [
            'INNISTAR',
            'Đánh giá sản phẩm',
            'Đăng ký nhận mẫu thử',
            'Câu hỏi thường gặp',
        ],
        // "Sự kiện": [
        //     'Ưu đãi hiện tại',
        //     'Ưu đãi kết thúc',
        //     'Kết quả sự kiện',
        // ]
    },
    {
        "Về Innifree": [
            'Câu chuyện thương hiệu',
            'Quá trình phát triển',
            'Câu chuyện thành phần',
        ],
        // "Thành viên": [],
        // "Tuyển dụng": [],
    },
    {
        "Trung tâm khách hàng": [
            'Mua hàng và Ưu đãi thành viên',
            'Hỗ trợ',
            'Hệ thống cửa hàng',
            'Thông báo',
            'Câu hỏi thường gặp',
        ],
        // "": ['Đăng nhập', 'Đăng ký thành viên'],
        // "SITE MAP": []
    }
]

const Footer = () => {
    return (
        <div className="footer">
            <div className="footer-top">
                <div className="footer-logo">
                    <div className="footer-logo-1">
                        <img src={logo} alt="logo" />
                        <ul>
                            <a href="#" className="icon">
                                <li>
                                    <img src={facebook} alt="logo" />
                                </li>
                            </a>
                            <a href="#" className="icon">
                                <li>
                                    <img src={youtube} alt="logo" />
                                </li>
                            </a>
                            <a href="#" className="icon">
                                <li>
                                    <img src={instagram} alt="logo" />
                                </li>
                            </a>
                        </ul>
                    </div>
                    {/* <div className="footer-logo-2">
                        <img src={footMark} alt="foot-mark" />
                        <div className="foot-d">
                            &copy; 2020 beautifull Inc.
                            <br />
                            All rights reserved
                        </div>
                    </div> */}
                </div>
                <div className="footer-info">
                    {Object.keys(footerTopDetail).map((index) =>
                        <div key={index} className="footer-des">
                            {Object.keys(footerTopDetail[index]).map((key) => {
                                return <div key={key} className="footer-des-wrap">
                                    <h6>{key}</h6>
                                    <ul>
                                        {Object.keys(footerTopDetail[index][key]).map((indexItem) => {
                                            return <li key={indexItem}>
                                                <a href="#" className="footer-prod">{footerTopDetail[index][key][indexItem]}</a>
                                            </li>
                                        })}
                                    </ul>
                                </div>
                            })}
                        </div>
                    )}
                </div>
            </div>
            {/* <div className="footer-bot">
                <div className="footer-bot-1">
                    <ul>
                        <li>
                            <a href="#">Chính sách giao hàng và thanh toán</a>
                        </li>
                        <li className="dash">
                            <a href="#" className="active ">Chính sách bảo mật và thông tin</a>
                        </li>
                        <li className="dash">
                            <a href="#">Chính sách mua hàng</a>
                        </li>
                        <li className="dash">
                            <a href="#">Chính sách trả hàng</a>
                        </li>
                    </ul>
                </div>
            </div> */}
        </div>
    )
}

export default Footer;