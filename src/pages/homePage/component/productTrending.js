import React from "react";
import { useState, useEffect } from "react";
import { caretRightFill, caretLeftFill } from "assets/icon/icon";
import { GetListProductTrending } from "../../../services/product";
import {
  CatalogContent,
  CatalogDescriptionContent,
} from "component/common/components/catalog/catalog";
import Slider from "react-slick";
import BoundingBox from "component/common/components/box/box";
import prod1 from "assets/img/product/prod_1_1.png";
import prod2 from "assets/img/product/prod_1_2.png";
import { ACCEPT } from "../../../constants/const";
import prod3 from "assets/img/product/prod_1_3.png";
import { settings } from "./utils";

const detail = [
  {
    img: prod1,
  },
  {
    img: prod2,
  },
  {
    img: prod3,
  },
  {
    img: prod1,
  },
  {
    img: prod2,
  },
  {
    img: prod3,
  },
  {
    img: prod1,
  },
];

const ListProductPreferentailComponent = (props) => {
  const [products, setProducts] = useState({});

  const getListProductTreding = async () => {
    let err;
    const respProduct = await GetListProductTrending().catch(
      (errOut) => {
        err = errOut;
      }
    );
    if (err || respProduct.status_code != ACCEPT) {
      return;
    }
    console.log("product_trending:",respProduct.product_trending)
    setProducts(respProduct.product_trending);
  };

  useEffect(() => {
    getListProductTreding();
  }, []);

  return (
    <CatalogContent title={"SẢN PHẨM BÁN CHẠY"}>
      <CatalogDescriptionContent>
        <Slider {...settings}>
          {Object.keys(products || []).map((index) => {
            return (
              <BoundingBox
                tag={products[index].tag}
                // TODO change image
                img={detail[index].img}
                alt={index}
                description={products[index].name}
                amount={products[index].amount}
                key={index}
                // handleModal={props.setVisible}
                productID={products[index].product_id}
                />
            );
          })}
        </Slider>
      </CatalogDescriptionContent>
    </CatalogContent>
  );
};

export default ListProductPreferentailComponent;
